
var inicio = new Vue({
	el: "#inicio",
	data: {
		vm: null,
		produto: {
			nome: '',
			volume: 0.0,
			fabricante: { id: 0, nome: '' },
			unidade: '',
			estoque: 0,
		},
		mensagem: "",
		nome: '',
		volume: '',
		fabricanteNome: '',
		unidade: '',
		estoque: '',
		show: false,
		nomeState: null,
		fabricanteState: null,
		volumeState: null,
		unidadeState: null,
		estoqueState: null,


		listaProdutos: [],

	},
	created: function () {
		vm = this;
		vm.buscaProdutos();
		// vm.adicionar();
	},
	computed: {

	},
	methods: {
		checkFormValidity() {
			const valid = vm.$refs.form.checkValidity();
			vm.nomeState = valid ? 'valid' : 'invalid';
			vm.fabricanteState = valid ? 'valid' : 'invalid';
			vm.volumeState = valid ? 'valid' : 'invalid';
			vm.unidadeState = valid ? 'valid' : 'invalid';
			vm.estoqueState = valid ? 'valid' : 'invalid';

			return valid
		},
		resetModal() {
			vm.nome = ''
			vm.nomeState = null
		},
		Salvar(bvModalEvt) {
			bvModalEvt.preventDefault()
			vm.adicionar()
		},
		adicionar: function () {
			vm.show = true;

			if (!vm.checkFormValidity()) {
				return
			}

			produto = {
				nome: vm.nome,
				volume: vm.volume,
				fabricante: { id: 1, nome: "OMO" },
				unidade: vm.unidade,
				estoque: vm.estoque,
			}

			const resultado = JSON.stringify(produto);
			console.log(resultado);
			axios.post("/mercado/rs/produtos", resultado)
				.then(response => {
					vm.buscaProdutos();
				}).catch(function (error) {
					console.log(error);
					vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
				}).finally(function () {


				});
			vm.mensagem = "cadastrado com sucesso"
			vm.$nextTick(() => {
				this.$refs.modal.hide()
			})




		},
		buscaProdutos: function () {
			axios.get("/mercado/rs/produtos")
				.then(response => {
					vm.listaProdutos = response.data;
				}).catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
				}).finally(function () { });
		},
		excluirProduto: function (index) {

			axios.delete("/mercado/rs/produtos/" + index)
				.then(response => {
					vm.listaProdutos.splice(index, -1);
					vm.buscaProdutos();
				}).catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
				}).finally(function () { });
		},
		alterarProduto: function (id) {

			axios.get("/mercado/rs/produtos/" + id)
				.then(response => {
					let data = response.data
					vm.nome = data.nome;
					vm.volume = data.volume;
					vm.unidade = data.unidade;
					vm.estoque = data.estoque;

					vm.buscaProdutos();
				}).catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
				}).finally(function () { });


		},
	}
});