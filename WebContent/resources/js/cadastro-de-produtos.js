var inicio = new Vue({
    el: "#inicioProduto",
    data: {
        vm: null,
        mensagem: "",
        nome: '',
        volume: '',
        fabricanteNome: '',
        unidade: '',
        estoque: '',
        show: false,
        nomeState: null,
        fabricanteState: null,
        volumeState: null,
        unidadeState: null,
        estoqueState: null,
        submittedNames: [],
        options: [{
                value: null,
                text: 'Please select an option'
            },
            {
                value: 'a',
                text: 'This is First option'
            },
            {
                value: 'b',
                text: 'Selected Option'
            },
            {
                value: 'd',
                text: 'This one is disabled',
                disabled: true
            }
        ]
    },
    created: function () {
        vm = this;

    },
    methods: {

        checkFormValidity() {
            const valid = vm.$refs.form.checkValidity();
            vm.nomeState = valid ? 'valid' : 'invalid';
            vm.fabricanteState = valid ? 'valid' : 'invalid';
            vm.volumeState = valid ? 'valid' : 'invalid';
            vm.unidadeState = valid ? 'valid' : 'invalid';
            vm.estoqueState = valid ? 'valid' : 'invalid';

            return valid
        },
        resetModal() {
            vm.nome = ''
            vm.nomeState = null
        },
        Salvar(bvModalEvt) {
            bvModalEvt.preventDefault()
            vm.adicionar()
        },
        adicionar: function () {

            vm.show = true;
            if (!vm.checkFormValidity()) {
                return
            }
            vm.mensagem = "cadastrado com sucesso"
            vm.submittedNames.push(this.nome)
            vm.$nextTick(() => {
                this.$refs.modal.hide()
            })

        },
    }
});