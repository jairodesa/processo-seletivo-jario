#!/bin/bash
mvn clean install -e -f  /usr/src/maven  &&
find . -name "*.war" -exec cp '{}' /usr/local/tomcat/webapps \;