package com.hepta.mercado.service.impl;

import java.util.List;


import com.hepta.mercado.entity.Produto;
import com.hepta.mercado.persistence.ProdutoDAO;
import com.hepta.mercado.service.IProdutoService;

public class ProdutoService implements IProdutoService {
   
	private ProdutoDAO dao;
	
	public ProdutoService() {
		dao = new ProdutoDAO();
	}
	
	@Override
	public void save(Produto produto) throws Exception {
		if(!produto.verificandoVulos())  throw new Exception("Não pode ter informações de produtos sem preencher");
		dao.save(produto);

	}

	@Override
	public Produto update(Produto produto,Integer id) throws Exception {
		if( dao.find(id) == null)  throw new Exception("Não existe id do produto");
		return dao.update(produto);
	}

	@Override
	public void delete(Integer id) throws Exception {
		if( dao.find(id) == null)  throw new Exception("Não existe id do produto");
	    dao.delete(id);
		
	}

	@Override
	public Produto find(Integer id) throws Exception {
		return dao.find(id);
	}

	@Override
	public List<Produto> getAll() throws Exception {
		return  dao.getAll();
	}

}
