package com.hepta.mercado.service;

import java.util.List;

import com.hepta.mercado.entity.Produto;

public interface IProdutoService {
	public void save(Produto produto) throws Exception;

	public Produto update(Produto produto, Integer id) throws Exception;

	public void delete(Integer id) throws Exception;

	public Produto find(Integer id) throws Exception;

	public List<Produto> getAll() throws Exception;

}
