package com.hepta.mercado.rest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.mercado.entity.Produto;
import com.hepta.mercado.service.IProdutoService;
import com.hepta.mercado.service.impl.ProdutoService;


@Path("/produtos")
public class ProdutoResource {
	
	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	private IProdutoService Service;
	
	public ProdutoResource() {
		Service = new ProdutoService();
	}
	
	protected void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	/**
	 * Adiciona novo produto no mercado
	 * 
	 * @param produto: Novo produto
	 * @return response 200 (OK) - Conseguiu adicionar
	 * @throws Exception 
	 */
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public Response produtoCreate(Produto produto) throws Exception {
		try {
			Service.save(produto);
		} catch ( SQLException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao cadastrar produtos " + e.getMessage()).build();
		}
		return Response.status(Status.CREATED).build();
	}
	
	/**
	 * Lista todos os produtos do mercado
	 * 
	 * @return response 200 (OK) - Conseguiu listar
	 */
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response produtoRead() {
		List<Produto> produtos = new ArrayList<>();
		try {
			produtos = Service.getAll();
		} catch(Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar produtos").build();
		}
		
		GenericEntity<List<Produto>> entity = new GenericEntity<List<Produto>>(produtos) {};
		return Response.status(Status.OK).entity(entity).build();
	}
	
	/**
	 * Lista todos os produtos do mercado
	 * 
	 * @return response 200 (OK) - Conseguiu listar
	 * @throws Exception 
	 */
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response produtoById(@PathParam("id") Integer id) throws Exception {
		Produto produtos =  Service.find(id);
		if(produtos == null) return Response.status(Status.NOT_FOUND).entity("Não existe id do produto").build();
		GenericEntity<Produto> entity = new GenericEntity<Produto>(produtos) {};
		return Response.status(Status.OK).entity(entity).build();
	}
	/**
	 * Atualiza um produto no mercado
	 * 
	 * @param id: id do produto
	 * @param produto: Produto atualizado
	 * @return response 200 (OK) - Conseguiu atualiza
	 * @throws Exception 
	 */
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@PUT
	public Response produtoUpdate(@PathParam("id") Integer id, Produto produto) throws Exception {
		
		try {
			Service.update(produto, id);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar produtos" + e.getMessage()).build();
		}
		return Response.status(Status.NO_CONTENT).build();
	}
	
	/**
	 * Remove um produto do mercado
	 * 
	 * @param id: id do produto
	 * @return response 200 (OK) - Conseguiu remover
	 * @throws Exception 
	 */
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@DELETE
	public Response produtoDelete(@PathParam("id") Integer id) throws Exception {
		try {
			Service.delete(id);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar produtos:. ERRO: " + e.getMessage()).build();
		}
		return Response.status(Status.NO_CONTENT).build();
	}

}
