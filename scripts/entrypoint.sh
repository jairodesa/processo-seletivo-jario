#!/bin/bash
mvn clean install -e -f  /usr/src/maven  &&
find /usr/src/maven/target -name "*.war" -exec cp '{}' /usr/local/tomcat/webapps \;