package com.hepta.mercado.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.client.Entity;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.entity.Produto;
import com.hepta.mercado.persistence.ProdutoDAO;

class ProdutoResourceTest {

	private static List<Produto> produtos = new ArrayList<>();
	private static Client client;
	private static final String URL_LOCAL = "http://localhost:8080/mercado/rs/produtos";
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ClientConfig config = new ClientConfig();
		 client = ClientBuilder.newClient(config);
		 
	    ProdutoDAO dao = new ProdutoDAO();
		produtos = dao.getAll();
		
	}

	@Test
	void testListaTodosProdutos() {
		// QUANDO
		Response response = client.target( UriBuilder.fromUri(URL_LOCAL).build() ).request().get();
		// ENTAO
		assertEquals(Response.Status.OK.getStatusCode(),response.getStatusInfo().getStatusCode());
	}
	
	@Test
	void testListaProdutoPorId() {
		// QUANDO
		Response response = client.target( UriBuilder.fromUri(URL_LOCAL ).build() ).request().get();
		// ENTAO
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatusInfo().getStatusCode());
	}
	
	@Test
	void criarProduto() {
		Fabricante fabricante = new Fabricante(1, "OMO");
		Produto produto = new Produto("Sabão", fabricante, 5.0, "12", 15);

		// QUANDO
		Response response = client.target( UriBuilder.fromUri(URL_LOCAL).build() ).request().post(Entity.entity(produto, MediaType.APPLICATION_JSON));
		// ENTAO
		 assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatusInfo().getStatusCode());//(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}
	@Test
	void alterarProduto() {
		//Fabricante fabricante = new Fabricante(1, "OMO");
		Fabricante fabricante = new Fabricante(1, "OMO");
		Produto produto = new Produto("Sabão em pó", fabricante, 5.0, "12", 15);
		Integer id = produtos.get(0).getId();
		// QUANDO
		
		Response response = client.target( UriBuilder.fromUri(URL_LOCAL + "/" + id).build() ).request().put(Entity.entity(produto, MediaType.APPLICATION_JSON));
		// ENTAO
		System.out.println(response.getStatus());
		 assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatusInfo().getStatusCode());//(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}
	
	@Test
	void deleteProduto() {
		Integer id = produtos.get(produtos.size()-1).getId();
		// QUANDO
		Response response = client.target( UriBuilder.fromUri(URL_LOCAL + "/" + id).build() ).request().delete();
		// ENTAO
		 assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatusInfo().getStatusCode());//(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}
	
	@Test
	void deleteProdutoPassandoIdNaoExistente() {
	
		// QUANDO
		Response response = client.target( UriBuilder.fromUri(URL_LOCAL + "/0").build() ).request().delete();
		// ENTAO
		 assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatusInfo().getStatusCode());//(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}
	
	
	@Test
	void deleteProdutoEPassandoNullo() {
	
		// QUANDO
		Response response = client.target( UriBuilder.fromUri(URL_LOCAL + "/null").build() ).request().delete();
		// ENTAO
		 assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatusInfo().getStatusCode());//(response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode());
	}
	

}
